var businessLoans; //商业贷款
var loanTerm;  //贷款期限
var annualRate; //商业贷款年利率
var monthlyInterest_rate = annualRate*0.01/12;  //月利率

function Compute() {  //计算

    var businessLoans_text = document.getElementById("businessLoans").value;
    var loanTerm_text = document.getElementById("loanTerm").value;
    var annualRate_text = document.getElementById("annualRate").value;

    if(!Regex(businessLoans_text,loanTerm_text,annualRate_text))
    return;

    businessLoans = parseInt(businessLoans_text);  //商业贷款

    loanTerm = parseInt(loanTerm_text);  //贷款期限

    annualRate = parseFloat(annualRate_text);  //商业贷款年利率
    monthlyInterest_rate = annualRate*0.01/12;  //月利率


    var radio = document.getElementsByName("loanMethod");

    businessLoans = businessLoans*10000;
    loanTerm = loanTerm*12;

    if (radio[0].checked) {
        equal_loanPayment_Compute();  //等额本息
    } else {
        equal_principalPayment_Compute()  //等额本金
    }

}

// 等额本息计算方法
function equal_loanPayment_Compute() {
    let monthlyPayment = (businessLoans*monthlyInterest_rate*(Math.pow(1+monthlyInterest_rate,loanTerm)))/((Math.pow(1+monthlyInterest_rate,loanTerm))-1);  //每月还本付息金额
    let totalInterest = ((businessLoans*loanTerm*monthlyInterest_rate*(Math.pow(1+monthlyInterest_rate,loanTerm)))/((Math.pow(1+monthlyInterest_rate,loanTerm))-1))-businessLoans; //还款总利息
    let totalRepayment = (businessLoans*loanTerm*monthlyInterest_rate*(Math.pow(1+monthlyInterest_rate,loanTerm)))/((Math.pow(1+monthlyInterest_rate,loanTerm))-1);  //还款总额
    console.log(monthlyPayment,totalInterest,totalRepayment);

    document.getElementById("monthlyPayment")[0] = new Option("每月还本付息金额:"+monthlyPayment.toFixed(2));

    document.getElementById("monthlyPrincipal").removeAttribute("disabled");
    document.getElementById("monthlyInterest").removeAttribute("disabled");

    for(let i=1;i<=loanTerm;i++){
        let monthlyInterest = businessLoans*monthlyInterest_rate; //贷款每月利息
        let monthlyPrincipal = monthlyPayment-monthlyInterest; //贷款每月本金
        businessLoans = businessLoans - monthlyPrincipal;
        document.getElementById("monthlyPrincipal").add(new Option("第"+i+"期："+monthlyPrincipal.toFixed(2)+"元"));
        document.getElementById("monthlyInterest").add(new Option("第"+i+"期："+monthlyInterest.toFixed(2)+"元"));
    }

    document.getElementById("totalInterest").value = "还款总利息:"+totalInterest.toFixed(2)+"元";

    document.getElementById("totalRepayment").value = "还款总额:"+totalRepayment.toFixed(2)+"元";
}

// 等额本金计算方法
function equal_principalPayment_Compute() {
    let monthlyPrincipal = (businessLoans)/(loanTerm); //每月本金
    let totalInterest = (loanTerm+1)*businessLoans*monthlyInterest_rate/2; //还款总利息
    let totalRepayment = (loanTerm+1)*businessLoans*monthlyInterest_rate/2+businessLoans;  //还款总额
    console.log(monthlyPrincipal,totalInterest,totalRepayment);

    document.getElementById("monthlyPrincipal")[0] = new Option("每期本金:"+monthlyPrincipal.toFixed(2));

    document.getElementById("monthlyPayment").removeAttribute("disabled");
    document.getElementById("monthlyInterest").removeAttribute("disabled");

    for(let i=0;i<loanTerm;i++){
        let monthlyInterest = (businessLoans-monthlyPrincipal*i)*monthlyInterest_rate; //贷款每月利息
        let monthlyPayment =  monthlyPrincipal + monthlyInterest; //贷款每月还本付息金额
        document.getElementById("monthlyPayment").add(new Option("第"+(i+1)+"期："+monthlyPayment.toFixed(2)+"元"));
        document.getElementById("monthlyInterest").add(new Option("第"+(i+1)+"期："+monthlyInterest.toFixed(2)+"元"));
    }
    document.getElementById("totalInterest").value = "还款总利息:"+totalInterest.toFixed(2)+"元";

    document.getElementById("totalRepayment").value = "还款总额:"+totalRepayment.toFixed(2)+"元";


    // alert("在贷款"+businessLoans*10000+"元，期限为"+loanTerm+"年，年利率为"+annualRate+"%的情况下"+
    // "\n以等额本金还款情况如下：\n"+
    // "每月还款本金 :"+monthlyPrincipal.toFixed(2)+
    // "元\n还款总利息"+totalInterest.toFixed(2)+
    // "元\n还款总额"+totalRepayment.toFixed(2)+"元");
}

// 正则表达式
function Regex(businessLoans_text,loanTerm_text,annualRate_text) {

    var patternBusinessLoans = /^[a-z0-9_\u4e00-\u9fa5]{1,10}$/;
    var patternLoanTerm = /^[0-9_]{1,5}$/;
    var patternAnnualRate = /^[0-9.]{1,12}$/;

    if (businessLoans_text == "") {
        alert("商业贷款:(万元)未输入，请输入值！");
        return false;
    }

    if (loanTerm_text == "") {
        alert("商业贷款期限:(年)未输入，请输入值！");
        return false;
    }

    if (annualRate_text == "") {
        alert("商业贷款年利率:(%)未输入，请输入值！");
        return false;
    }


    if (patternBusinessLoans.test(businessLoans_text)) {
        if(patternLoanTerm.test(loanTerm_text)) {
            if(patternAnnualRate.test(annualRate_text))
            return true;
            else {
                alert("商业贷款年利率:(%)格式错误，请重新输入！");
                return false;
            }
        } else {
            alert("商业贷款期限:(年)格式错误，请重新输入！");
            return false;
        }
        }else {
            alert("商业贷款:(万元)格式错误，请重新输入！");
            return false;
        }
}

$(document).ready(function(){
    $('input[type=radio][name=loanMethod]').click(function() {
        $("#wrapper-result").load(location.href+" #result")
    });
    $( "#datepicker" ).datepicker();
});
