
 function getResult(id){
  var n1 = parseFloat(document.getElementById("n1").value);
  var n2 = parseFloat(document.getElementById("n2").value);
  var result = "";
  if(isNaN(n1) || isNaN(n2))
        {
            alert('请输入数字！');
            return false;
        }
  switch(id)
        {
            case '+':
                result=n1+n2;
                break;
            case '-':
                result=n1-n2;
                break;
            case '*':
                result=n1*n2;
                break;
            case '/':
                if('0'==n2)
                {
                    alert('分母不能为零！');
                    return false;
                }
                result=n1/n2;
                break;
        }
  document.getElementById("n3").value = result
 }


class Calculator {
    constructor(n1,n2) {
        this.n1 = n1;
        this.n2 = n2;
    }

    getResult(id) {
        var result = "";
        if(isNaN(this.n1) || isNaN(this.n2)) {
                alert('请输入数字！');
                return false;
            }
        switch(id) {
                case '+':
                    result=this.n1+this.n2;
                    break;
                case '-':
                    result=this.n1-this.n2;
                    break;
                case '*':
                    result=this.n1*this.n2;
                    break;
                case '/':
                    if('0'==this.n2)
                    {
                        alert('分母不能为零！');
                        return false;
                    }
                    result=this.n1/this.n2;
                    break;
            }
        document.getElementById("n3").value = result
    }
}

function Calc(id) {
    const calc = new Calculator(parseFloat(document.getElementById("n1").value),parseFloat(document.getElementById("n2").value));
    calc.getResult(id);
}