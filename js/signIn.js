
// 登录
function login() {
    var patternUsername = /^[A-Za-z0-9_\u4e00-\u9fa5]{2,10}$/;//汉字+数字+字母+下划线组合,2-10个字符
    var patternPassword = /^[A-Za-z0-9_]{5,12}$/;//数字+字母+下划线,5-12个字符
    //表单提交前的验证参数
    var usernameInputOK = false;
    var passwordInputOK = false;
    if (username.value == "") {
        alert("用户名不能为空");
        usernameInputOK = false;
    }
    else if (patternUsername.test(username.value)){
        usernameInputOK = true;
        if (password.value == "") {
            alert("密码不能为空");
            passwordInputOK = false;
        }else if (patternPassword.test(password.value)){
            if(password.value==again.value) {
                passwordInputOK = true;
            }
        }
        else{
            alert("密码可使用数字+字母+下划线,5-12个字符");
            passwordInputOK = false;
        }
    }
    else{
        alert("用户名可使用汉字+数字+字母+下划线组合,2-10个字符");
        usernameInputOK = false;
    }

    if(passwordInputOK&&usernameInputOK){
        alert("登录成功");
        window.location.replace("main.html");
    }
}



// 注册
function register() {
    var patternUsername = /^[A-Za-z0-9_\u4e00-\u9fa5]{2,10}$/;//汉字+数字+字母+下划线组合,2-10个字符
    var patternPassword = /^[A-Za-z0-9_]{5,12}$/;//数字+字母+下划线,5-12个字符
    //表单提交前的验证参数
    console.log(username,password,again);
    var usernameInputOK = false;
    var passwordInputOK = false;
    if (username.value == "") {
        alert("用户名不能为空");
        usernameInputOK = false;
    }
    else if (patternUsername.test(username.value)){
        usernameInputOK = true;
        if (password.value == "") {
            alert("密码不能为空");
            passwordInputOK = false;
        }else if (patternPassword.test(password.value)){
            if(password.value==again.value) {

                passwordInputOK = true;
            }
            else {
                console.log(password.value,confirm.value)
                alert("两次密码不一致");
            }
        }
        else{
            alert("密码可使用数字+字母+下划线,5-12个字符");
            passwordInputOK = false;
        }
    }
    else{
        alert("用户名可使用汉字+数字+字母+下划线组合,2-10个字符");
        usernameInputOK = false;

    }

    if(passwordInputOK&&usernameInputOK){
        alert("注册成功");
        window.location.replace("login.html");
    }

}