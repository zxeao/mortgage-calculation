var businessLoans; //商业贷款
var loanTerm;  //贷款期限
var annualRate; //商业贷款年利率
var monthlyInterest_rate = annualRate*0.01/12;  //月利率

function Compute() {
    var businessLoans_text = document.getElementById("businessLoans").value;

    businessLoans = parseInt(businessLoans_text);  //商业贷款


    var loanTerm_text = document.getElementById("loanTerm").value;

    loanTerm = parseInt(loanTerm_text);  //贷款期限


    var annualRate_text = document.getElementById("annualRate").value;

    annualRate = parseFloat(annualRate_text);  //商业贷款年利率
    monthlyInterest_rate = annualRate*0.01/12;  //月利率


    var radio = document.getElementsByName("loanMethod");

    if (radio[0].checked) {
        equal_loanPayment_Compute();  //等额本息
    } else {
        equal_principalPayment_Compute()  //等额本金
    }

}

// 等额本息计算方法
function equal_loanPayment_Compute() {
    let monthlyPayment = (businessLoans*10000*monthlyInterest_rate*(Math.pow(1+monthlyInterest_rate,loanTerm*12)))/((Math.pow(1+monthlyInterest_rate,loanTerm*12))-1);  //每月还本付息金额
    let totalInterest = ((businessLoans*10000*loanTerm*12*monthlyInterest_rate*(Math.pow(1+monthlyInterest_rate,loanTerm*12)))/((Math.pow(1+monthlyInterest_rate,loanTerm*12))-1))-businessLoans*10000; //还款总利息
    let totalRepayment = (businessLoans*10000*loanTerm*12*monthlyInterest_rate*(Math.pow(1+monthlyInterest_rate,loanTerm*12)))/((Math.pow(1+monthlyInterest_rate,loanTerm*12))-1);  //还款总额
    console.log(monthlyPayment,totalInterest,totalRepayment);
    alert("每月还本付息金额 :"+monthlyPayment+"\n还款总利息"+totalInterest+"\n还款总额"+totalRepayment);
}

// 等额本金计算方法
function equal_principalPayment_Compute() {
    let monthlyPrincipal = (businessLoans*10000)/(loanTerm*12); //每月本金
    let totalInterest = (loanTerm*12+1)*businessLoans*10000*monthlyInterest_rate/2; //还款总利息
    let totalRepayment = (loanTerm*12+1)*businessLoans*10000*monthlyInterest_rate/2+businessLoans*10000;  //还款总额
    console.log(monthlyPrincipal,totalInterest,totalRepayment);
    alert("每月本金 :"+monthlyPrincipal+"\n还款总利息"+totalInterest+"\n还款总额"+totalRepayment);
}